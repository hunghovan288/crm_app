import 'package:crm_app/presentation/journey/feature/calender/calender_screen.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/dashboard_screen.dart';
import 'package:crm_app/presentation/journey/feature/news/news_screen.dart';
import 'package:crm_app/presentation/journey/feature/personal/personal_screen.dart';
import 'package:crm_app/presentation/widgets/bottom_nagivation_widget.dart';
import 'package:crm_app/presentation/widgets/layout_contain_widget_keep_alive.dart';
import 'package:flutter/material.dart';

class ScreenContainer extends StatelessWidget {
  List<String> icons = [
    'dashboard.svg',
    'calender.svg',
    'note.svg',
    'personal.svg',
  ];

  void _addProject() {

  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigation(
      icons: icons,
      iconCenterTap: _addProject,
      tabViews: [
        LayoutContainWidgetKeepAlive(child: DashBoardScreen()),
        LayoutContainWidgetKeepAlive(child: CalenderScreen()),
        LayoutContainWidgetKeepAlive(child: NewsScreen()),
        LayoutContainWidgetKeepAlive(child: PersonalScreen()),
      ],
    );
  }
}
