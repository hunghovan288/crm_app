import 'package:crm_app/presentation/widgets/custom_appbar.dart';
import 'package:crm_app/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';

class PersonalScreen extends StatefulWidget {
  @override
  _PersonalScreenState createState() => _PersonalScreenState();
}

class _PersonalScreenState extends State<PersonalScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Container(
        child: Center(
          child: Text('Person Screen'),
        ),
      ),
    );
  }
}
