import 'package:crm_app/data/models/project_model.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:crm_app/presentation/widgets/circular_percent_indicator.dart';
import 'package:crm_app/presentation/widgets/custom_image_network.dart';
import 'package:flutter/material.dart';

const sizeImage = 40.0;

class DashBoardItemProject extends StatelessWidget {
  final ProjectModel projectModel;

  const DashBoardItemProject({Key key, this.projectModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final listImage = [
      'https://images.giaoducthoidai.vn/Uploaded/thoank/2019-11-20/1aa-COSY.jpg',
      'https://images.giaoducthoidai.vn/Uploaded/thoank/2019-11-20/h61574088345678165930722157412682866315741268286631928554206_UUDD.jpg',
      'https://images.giaoducthoidai.vn/Uploaded/thoank/2019-11-20/h41574088345666373498536157412683803615741268380361278817775_WEOP.jpg'
    ];
    List<Widget> images = [];
    for (int i = 0; i < listImage.length; i++) {
      if (i == 2) {
        images.add(_moreImage(marginLeft: 4 * sizeImage / 3));
        break;
      }
      if (i == 1) {
        images
            .add(_itemImage(url: listImage[i], marginLeft: 2 * sizeImage / 3));
        continue;
      }
      images.add(_itemImage(url: listImage[i], marginLeft: 0));
    }
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 6),
      decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius:
              BorderRadius.circular(DashboardConstant.borderItemWidget)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            projectModel.name ?? '',
            style: AppTextTheme.mediumBlack,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(height: 10),
          Text(
            projectModel.description ?? '',
            style: AppTextTheme.normalGrey,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(height: 10),
          Text(
            '4 days left',
            style: AppTextTheme.normalGrey8,
          ),
          const SizedBox(height: 10),
          Container(
            width: double.infinity,
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Stack(
                    children: images,
                  ),
                ),
                CircularPercentIndicator(
                  radius: sizeImage,
                  lineWidth: 5.0,
                  animation: true,
                  animationDuration: 700,
                  percent: 0.7,
                  animateFromLastPercent: true,
                  center: Text(
                    "${projectModel.percent.toInt()}%",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 11.0),
                  ),
                  circularStrokeCap: CircularStrokeCap.round,
                  progressColor: AppColors.primaryColor,
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _itemImage({double marginLeft, String url}) => Positioned(
        left: marginLeft ?? 0,
        child: Stack(
          children: [
            CustomImageNetwork(
              url: url,
              width: sizeImage,
              height: sizeImage,
              border: sizeImage / 2,
              fit: BoxFit.cover,
            ),
            Container(
              width: sizeImage,
              height: sizeImage,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: AppColors.white, width: 1.5)),
            )
          ],
        ),
      );

  Widget _moreImage({double marginLeft}) => Positioned(
        left: marginLeft ?? 0,
        child: Container(
          width: sizeImage,
          height: sizeImage,
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: AppColors.grey7),
          child: Center(
            child: Icon(
              Icons.more_horiz,
              color: AppColors.white,
            ),
          ),
        ),
      );
}
