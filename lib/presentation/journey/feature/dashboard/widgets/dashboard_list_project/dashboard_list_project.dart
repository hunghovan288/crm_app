import 'package:crm_app/data/models/project_model.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/dashboard_list_project/dashboard_item_project.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class DashboardListProject extends StatelessWidget {
  final String title;
  final Function onSeeAll;
  final List<ProjectModel> projects;
  final int projectStatus;

  const DashboardListProject({
    Key key,
    this.title,
    this.onSeeAll,
    this.projects,
    this.projectStatus,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          Row(
            children: [
              Text(
                title ?? '',
                style: AppTextTheme.mediumBlack.copyWith(
                  fontSize: DashboardConstant.sizeTitleLayoutInDashboardScreen,
                ),
              ),
              const Spacer(),
              InkWell(
                onTap: onSeeAll,
                child: Padding(
                  padding: const EdgeInsets.only(left: 12, top: 4, bottom: 4),
                  child: Text(
                    translate(DashboardConstant.viewAll),
                    style: AppTextTheme.normalGrey
                        .copyWith(decoration: TextDecoration.underline),
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Expanded(
                    child: DashBoardItemProject(
                  projectModel: ProjectModel(
                      percent: 40,
                      description: 'Hello DashBoardItemProject description',
                      status: projectStatus ,
                      name: 'DashBoardItemProject'),
                )),
                const SizedBox(width: 16),
                Expanded(
                    child: DashBoardItemProject(
                  projectModel: ProjectModel(
                      percent: 80,
                      description: 'Hello DashBoardItemProject description',
                      status: projectStatus ,
                      name: 'DashBoardItemProject'),
                )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
