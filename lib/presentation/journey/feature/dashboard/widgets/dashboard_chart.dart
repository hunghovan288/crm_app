import 'package:crm_app/presentation/journey/feature/dashboard/widgets/custom_circle_chart.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';

class DashboardChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: 16),
        Stack(
          alignment: AlignmentDirectional.center,
          children: [
            CustomCircleChart(
              dataMap: {'1': 0.2, '2': 0.5,'3':0.3},
              listColor: [AppColors.primaryColor, AppColors.yellow,AppColors.green],
            ),
            Column(
              children: [
                Text(
                  '26',
                  style: AppTextTheme.medium20PxBlack,
                ),
                Text(
                  'Total Project',
                  style: AppTextTheme.normalGrey6,
                )
              ],
            )
          ],
        ),
         const SizedBox(width: 70),
        Expanded(
          child: Column(
            children: [
              _itemInfoChart(
                  color: AppColors.primaryColor, percent: 0.4, status: 'Done'),
             const SizedBox(height: 16),
              _itemInfoChart(
                  color: AppColors.yellow, percent: 0.2, status: 'To Do'),
              const SizedBox(height: 16),
              _itemInfoChart(
                  color: AppColors.green, percent: 0.4, status: 'Pending'),
            ],
          ),
        ),

      ],
    );
  }

  Widget _itemInfoChart({Color color, double percent, String status}) =>
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: 6,
            height: 6,
            margin: EdgeInsets.only(right: 8,top: 10),
            decoration: BoxDecoration(
              color: color,
              shape: BoxShape.circle,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Text(
                '$percent%',
                style: AppTextTheme.normalGrey9.copyWith(fontSize: 18),
              ),
              Text(
                status ?? '',
                style: AppTextTheme.normalGrey,
              )
            ],
          )
        ],
      );
}
