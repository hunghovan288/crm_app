import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:crm_app/presentation/widgets/bottom_sheet_container.dart';
import 'package:flutter/material.dart';

class DashboardBottomSheetSelectTypeProject extends StatefulWidget {
  final Function(String time) onItemTimeTab;
  final String inputTime;

  const DashboardBottomSheetSelectTypeProject(
      {Key key, this.onItemTimeTab, this.inputTime})
      : super(key: key);

  @override
  _DashboardBottomSheetSelectTypeProjectState createState() =>
      _DashboardBottomSheetSelectTypeProjectState();
}

class _DashboardBottomSheetSelectTypeProjectState
    extends State<DashboardBottomSheetSelectTypeProject> {
  String _timeSelected;

  @override
  void initState() {
    _timeSelected = widget.inputTime ?? 'Overall';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheetContainer(
      onLeftTap: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          _item(
            'Overall',
            _timeSelected,
            onTap: widget.onItemTimeTab,
          ),
          _item(
            'Overall2',
            _timeSelected,
            onTap: widget.onItemTimeTab,
          ),
          _item(
            'Overall3',
            _timeSelected,
            onTap: widget.onItemTimeTab,
          ),
        ],
      ),
    );
  }

  Widget _item(String time, String timeSelected,
          {Function(String time) onTap}) =>
      InkWell(
        onTap: () {
          Navigator.pop(context);
          onTap(time);
        },
        child: SizedBox(
          height: 56,
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Spacer(),
              Row(
                children: [
                  SizedBox(width: 16.0),
                  Expanded(
                    child: Text(
                      time,
                      textAlign: TextAlign.start,
                      style: AppTextTheme.normalGrey9,
                    ),
                  ),
                  time == timeSelected
                      ? Icon(
                          Icons.check,
                          color: AppColors.primaryColor,
                        )
                      : SizedBox(),
                  SizedBox(width: 16.0),
                ],
              ),
              const Spacer(),
              Divider(
                height: 1.0,
                color: Colors.grey[300],
              )
            ],
          ),
        ),
      );
}
