import 'package:crm_app/common/utils/common_util.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/dashboard_bottom_sheet_select_type_project.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';

class DashboardSelectTypeProject extends StatefulWidget {
  @override
  _DashboardSelectTypeProjectState createState() =>
      _DashboardSelectTypeProjectState();
}

class _DashboardSelectTypeProjectState
    extends State<DashboardSelectTypeProject> {
  String _typeProjectSelected = 'Overall';

  void _onSelectType() {
    CommonUtil.showCustomBottomSheet(
        context: context,
        height: 250,
        child: DashboardBottomSheetSelectTypeProject(
          inputTime: _typeProjectSelected,
          onItemTimeTab: (type) {
            setState(() {
              _typeProjectSelected = type;
            });
          },
        ));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onSelectType,
      child: Container(
        height: 40,
        decoration: BoxDecoration(
          color: AppColors.backgroundGrey,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              Text(
                _typeProjectSelected,
                style: AppTextTheme.normalGrey9,
              ),
              Icon(
                Icons.arrow_drop_down,
                color: AppColors.grey7,
              )
            ],
          ),
        ),
      ),
    );
  }
}
