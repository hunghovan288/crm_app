import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:crm_app/common/extensions/screen_extension.dart';

class CustomCircleChart extends StatelessWidget {
  final Map<String, double> dataMap;
  final List<Color> listColor;

  const CustomCircleChart({Key key, this.dataMap, this.listColor}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return PieChart(
      dataMap: dataMap,
      animationDuration: Duration(milliseconds: 800),
      chartLegendSpacing: 32,
      chartRadius: 160.w,
      colorList: listColor,
      initialAngleInDegree: 0,
      chartType: ChartType.ring,
      ringStrokeWidth: 25,
      legendOptions: LegendOptions(
        showLegendsInRow: false,
        legendPosition: LegendPosition.right,
        showLegends: false,
        legendShape: BoxShape.circle,
        legendTextStyle: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      chartValuesOptions: ChartValuesOptions(
        showChartValueBackground: true,
        showChartValues: false,
        showChartValuesInPercentage: false,
        showChartValuesOutside: false,
      ),
    );
  }
}
