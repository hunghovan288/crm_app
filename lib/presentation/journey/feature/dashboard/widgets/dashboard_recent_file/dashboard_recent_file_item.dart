import 'package:crm_app/data/models/file_model.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DashBoardRecentFileItem extends StatelessWidget {
  final FileModel fileModel;

  const DashBoardRecentFileItem({Key key, this.fileModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius:
              BorderRadius.circular(DashboardConstant.borderItemWidget)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            fileModel.icon,
            width: 50,
            height: 80,
          ),
          const SizedBox(width: 12),
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                fileModel.name ?? '',
                style: AppTextTheme.mediumBlack,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: 12),
              Row(
                children: [
                  Text(
                    fileModel.dateTime ?? '',
                    style: AppTextTheme.normalGrey,
                  ),
                  const SizedBox(width: 20),
                  Text(
                    fileModel.size ?? '',
                    style: AppTextTheme.normalGrey9,
                  )
                ],
              )
            ],
          ))
        ],
      ),
    );
  }
}
