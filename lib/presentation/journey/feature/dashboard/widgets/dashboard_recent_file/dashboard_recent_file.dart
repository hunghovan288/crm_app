import 'package:crm_app/common/constant/icon_constant.dart';
import 'package:crm_app/data/models/file_model.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/dashboard_recent_file/dashboard_recent_file_item.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';

class DashBoardRecentFile extends StatefulWidget {
  @override
  _DashBoardRecentFileState createState() => _DashBoardRecentFileState();
}

class _DashBoardRecentFileState extends State<DashBoardRecentFile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Recent Files',
            style: AppTextTheme.mediumBlack.copyWith(
              fontSize: DashboardConstant.sizeTitleLayoutInDashboardScreen,
            ),
          ),
          const SizedBox(height: 20),
          DashBoardRecentFileItem(
            fileModel: FileModel(
              dateTime: '19/02/2021',
              icon: IconConstant.word,
              name: 'Feedback of Customer',
              size: '4.3MB',
            ),
          ),
          const SizedBox(height: 20),
          DashBoardRecentFileItem(
            fileModel: FileModel(
              dateTime: '19/02/2021',
              icon: IconConstant.powerpoint,
              name: 'Feedback of Customer',
              size: '4.3MB',
            ),
          ),
          const SizedBox(height: 20),
          DashBoardRecentFileItem(
            fileModel: FileModel(
              dateTime: '19/02/2021',
              icon: IconConstant.excel,
              name: 'Feedback of Customer',
              size: '4.3MB',
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
