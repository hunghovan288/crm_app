import 'package:crm_app/common/constant/common_constant.dart';
import 'package:crm_app/common/constant/pixel_constant.dart';
import 'package:crm_app/common/utils/screen_utils.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/dashboard_constant.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/custom_circle_chart.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/custom_circle_chart/size_const.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/dashboard_chart.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/dashboard_list_project/dashboard_list_project.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/dashboard_recent_file/dashboard_recent_file.dart';
import 'package:crm_app/presentation/journey/feature/dashboard/widgets/dashboard_select_type_project.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:crm_app/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class DashBoardScreen extends StatefulWidget {
  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  @override
  Widget build(BuildContext context) {
    SizeUtil.getInstance().logicSize = MediaQuery.of(context).size;
    SizeUtil.initDesignSize();
    return CustomScaffold(
      body: Container(
        color: AppColors.white,
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            SizedBox(height: ScreenUtil.statusBarHeight + 20),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: PixelConstant.marginHorizontal),
              child: Row(
                children: [
                  Text(
                    translate(DashboardConstant.dashboard),
                    style: AppTextTheme.medium20PxBlack,
                  ),
                  const Spacer(),
                  DashboardSelectTypeProject(),
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 30,
                          horizontal: PixelConstant.marginHorizontal),
                      child: DashboardChart(),
                    ),
                    Container(
                      color: AppColors.backgroundGrey,
                      padding: EdgeInsets.symmetric(
                          horizontal: PixelConstant.marginHorizontal,
                          vertical: 20),
                      width: double.infinity,
                      child: Column(
                        children: [
                          DashboardListProject(
                            title: 'To do Project',
                            projectStatus: CommonConstant.PROJECT_TODO,
                          ),
                          const SizedBox(height: 20),
                          DashboardListProject(
                            title: 'Doing Project',
                            projectStatus: CommonConstant.PROJECT_DOING,
                          ),
                          const SizedBox(height: 20),
                          DashboardListProject(
                            title: 'Done Project',
                            projectStatus: CommonConstant.PROJECT_DONE,
                          ),
                          const SizedBox(height: 20),
                          DashboardListProject(
                            title: 'Pending Project',
                            projectStatus: CommonConstant.PROJECT_PENDING,
                          ),
                          const SizedBox(height: 20),
                          DashBoardRecentFile(),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
