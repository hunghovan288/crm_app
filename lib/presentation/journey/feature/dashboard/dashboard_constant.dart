class DashboardConstant{
  // text
  static const String path = 'DashboardConstant.';
  static const String dashboard = '${path}dashboard';
  static const String viewAll = '${path}view_all';

  // size
  static const double marginTopOfCircleChart = 250;
  static const double borderItemWidget = 10;
  static const double sizeTitleLayoutInDashboardScreen = 18;

}