import 'package:flutter/material.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';

class AppTextTheme {
  static const style10pxGrey = TextStyle(
    fontSize: 10.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey7,
  );
  static const style12pxGrey = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey7,
  );
  static const style12pxGrey6 = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey7,
  );
  static const style12pxGrey8 = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey8,
  );
  static const style10pxPrimary = TextStyle(
    fontSize: 10.0,
    fontWeight: FontWeight.w400,
    color: AppColors.primaryColor,
  );
  static const normalGrey5 = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey5,
  );
  static const normalGrey6 = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey6,
  );
  static const normalGrey = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey7,
  );
  static const normalPink= TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.pink,
  );
  static const normalGreen = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.green,
  );
  static const normalYellow = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.yellow,
  );
  static const normalBlue = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.blue,
  );
  static const normalWhite = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.white,
  );
  static const normalPrimary = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.primaryColor,
  );
  static const normalGrey8 = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey8,
  );
  static const normalGrey9 = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey9,
  );
  static const title = TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.w500,
    color: AppColors.grey9,
  );
  static const bigTitle = TextStyle(
    fontSize: 38.0,
    fontWeight: FontWeight.w500,
    color: AppColors.grey9,
  );
  static const medium = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    color: Colors.white,
  );
  static const medium20PxBlack = TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.w500,
    color: AppColors.grey9,
  );
  static const mediumBlack = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    color: AppColors.grey9,
  );
  static const mediumBlack14px = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
    color: AppColors.grey9,
  );
  static const mediumPink = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    color: AppColors.pink,
  );
  static const mediumPrimary = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.w500,
    color: AppColors.primaryColor,
  );
  static const smallGrey = TextStyle(
    fontSize: 12.0,
    fontWeight: FontWeight.w400,
    color: AppColors.grey7,
  );
}
