import 'package:crm_app/common/navigation/route_names.dart';
import 'package:crm_app/common/utils/screen_utils.dart';
import 'package:crm_app/presentation/routes.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/localized_app.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    final localizationDelegate = LocalizedApp.of(context).delegate;
    return MaterialApp(
      navigatorKey: Routes.instance.navigatorKey,
      debugShowCheckedModeBanner: false,
      title: 'CRM Application',
      onGenerateRoute: Routes.generateRoute,
      initialRoute: RouteName.dashBoardScreen,
      theme: ThemeData(
          primaryColor: AppColors.primaryColor,
          fontFamily: 'Roboto',
          canvasColor: Colors.transparent,
            platform: TargetPlatform.iOS,
          pageTransitionsTheme: const PageTransitionsTheme(builders: {
            TargetPlatform.android: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          })),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        localizationDelegate
      ],
      supportedLocales: localizationDelegate.supportedLocales,
      locale: localizationDelegate.currentLocale,
      builder: (context, widget) {
        ScreenUtil.init(context);
        return widget;
      },
    );
  }
}
