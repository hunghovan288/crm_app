import 'package:crm_app/common/utils/common_util.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/widgets/custom_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

const double defaultAppbar = 56.0;

class CustomScaffold extends StatelessWidget {
  final Color backgroundColor;
  final CustomAppBar customAppBar;
  final Widget body;
  final bool autoDismissKeyboard;
  final bool resizeToAvoidBottomPadding;
  final bool resizeToAvoidBottomInset;

  const CustomScaffold({
    Key key,
    this.backgroundColor,
    this.customAppBar,
    this.body,
    this.autoDismissKeyboard = true,
    this.resizeToAvoidBottomPadding,
    this.resizeToAvoidBottomInset,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor ?? AppColors.white,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      resizeToAvoidBottomPadding: resizeToAvoidBottomPadding,
      body: Column(
        children: [
          customAppBar ?? const SizedBox(),
          Expanded(
            child: InkWell(
                onTap: autoDismissKeyboard
                    ? () {
                        if (autoDismissKeyboard) {
                          CommonUtil.dismissKeyBoard(context);
                        }
                      }
                    : null,
                child: body ?? const SizedBox()),
          )
        ],
      ),
    );
  }
}


