import 'package:crm_app/common/constant/icon_constant.dart';
import 'package:crm_app/common/constant/pixel_constant.dart';
import 'package:crm_app/common/utils/screen_utils.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

const double defaultAppbar = 56.0;

class CustomAppBar extends StatelessWidget {
  final Function onBack;
  final Widget widgetRight;

  const CustomAppBar({Key key, this.onBack, this.widgetRight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight),
      height: defaultAppbar + ScreenUtil.statusBarHeight,
      decoration: BoxDecoration(color: AppColors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: onBack,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: PixelConstant.marginHorizontal),
              child: Container(
                width: 56,
                height: 56,
                padding: EdgeInsets.all(5),
                child: SvgPicture.asset(
                  IconConstant.back,
                ),
              ),
            ),
          ),
          const Spacer(),
          widgetRight ??const SizedBox(),
        ],
      ),
    );
  }
}
