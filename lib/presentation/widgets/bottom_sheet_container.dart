import 'package:crm_app/common/constant/icon_constant.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:crm_app/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class BottomSheetContainer extends StatelessWidget {
  final Widget child;
  final String title;
  final Function onLeftTap;
  final Function onRightTap;
  final String textButtonRight;

  const BottomSheetContainer({
    Key key,
    this.child,
    this.title,
    this.onLeftTap,
    this.onRightTap,
    this.textButtonRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
        color: Colors.white,
      ),
      child: Column(
        children: [
          InkWell(
            onTap: onLeftTap,
            child: Container(
              width: double.infinity,
              height: 56,
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(12),
                      topLeft: Radius.circular(12))),
              child: Row(
                children: [
                  Container(
                    width: 56,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(12),
                      ),
                    ),
                    child: Center(
                        child: SvgPicture.asset(
                      IconConstant.iconClose,
                      width: 20,
                      height: 20,
                      color: AppColors.grey9,
                    )),
                  ),
                  Expanded(
                    child: Text(
                      title ?? '',
                      style: AppTextTheme.mediumBlack,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  InkWell(
                    onTap: onRightTap,
                    child: Container(
                      width: 56,
                      height: 60,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(12),
                        ),
                      ),
                      child: Center(
                          child: Text(
                        textButtonRight ?? '',
                        style: AppTextTheme.normalBlue,
                      )),
                    ),
                  )
                ],
              ),
            ),
          ),
          Divider(height: 1, color: AppColors.grey5),
          Expanded(child: child),
        ],
      ),
    );
  }
}
