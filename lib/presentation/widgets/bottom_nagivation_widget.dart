import 'package:crm_app/common/constant/common_constant.dart';
import 'package:crm_app/common/constant/icon_constant.dart';
import 'package:crm_app/common/utils/screen_utils.dart';
import 'package:crm_app/presentation/themes/theme_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

typedef OnTabChanged = void Function(int index);

class BottomNavigation extends StatefulWidget {
  final List<Widget> tabViews;
  final List<String> icons;
  final Color activeColor;
  final Color inActiveColor;
  final double iconSize;
  final OnTabChanged onTabChanged;
  final int initIndex;
  final int countItem;
  final Function iconCenterTap;
  BottomNavigation({
    Key key,
    @required this.tabViews,
    @required this.icons,
    this.initIndex = 0,
    this.activeColor = AppColors.primaryColor,
    this.inActiveColor = AppColors.grey6,
    this.iconSize = 24.0,
    this.countItem = 0,
    this.iconCenterTap,
    this.onTabChanged,
  })  : assert(tabViews != null, 'Tab view not be null'),
        assert(icons != null, 'Icons not be null'),
        super(key: key);

  @override
  State<StatefulWidget> createState() => BottomNavigationState();
}

class BottomNavigationState extends State<BottomNavigation> {
  int selectedIndex;
  List<String> _icon;

  @override
  void initState() {
    selectedIndex = widget.initIndex;
    _icon = widget.icons;
    super.initState();
  }

  void changeToTabIndex(int index) {
    // if (index == 1) {
    //   _icon[1] = IconConstant.categorySelected;
    // } else {
    //   _icon[1] = 'icon_category.svg';
    // }
    // if (index == 2) {
    //   _icon[2] = IconConstant.cartSelected;
    // } else {
    //   _icon[2] = 'icon_shop.svg';
    // }
    // if (index == 4) {
    //   _icon[index] = IconConstant.userSelected;
    // } else {
    //   _icon[4] = 'icon_personal.svg';
    // }
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final tabs = _icon.asMap().entries.map(
      (entry) {
        final index = entry.key;
        final source = entry.value;
        final isSelected = index == selectedIndex;
        return source != null
            ? Expanded(
                child: Material(
                  color: Colors.white,
                  child: InkWell(
                    highlightColor: AppColors.grey6,
                    splashColor: AppColors.grey6,
                    onTap: () {
                      if (!isSelected) {
                        changeToTabIndex(index);
                        if (widget.onTabChanged != null) {
                          widget.onTabChanged(selectedIndex);
                        }
                      }
                    },
                    child: Container(
                      height: 58.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            '${IconConstant.path}$source',
                            width: widget.iconSize,
                            height: widget.iconSize,
                            fit: BoxFit.cover,
                            color: isSelected
                                ? widget.activeColor
                                : widget.inActiveColor,
                          ),
                          isSelected ? Container(
                            width: 4,
                            height: 4,
                            margin: EdgeInsets.only(top: 4),
                            decoration: BoxDecoration(
                              color: AppColors.primaryColor,
                              borderRadius: BorderRadius.circular(2)
                            ),
                          ) : const SizedBox()
                        ],
                      ),
                    ),
                  ),
                ),
              )
            : const Spacer();
      },
    ).toList();
    tabs.insert(2, _centerIconWidget());
    return Scaffold(
      body: IndexedStack(
        index: selectedIndex,
        children: widget.tabViews,
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: Row(children: tabs),
      ),
    );
  }

  Widget _centerIconWidget() {
    final screenWidget = ScreenUtil.screenWidthDp;
    return Container(
      width: screenWidget / 5,
      height: 40,
      child: Center(
        child: InkWell(
          onTap: widget.iconCenterTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                  color: AppColors.primaryColor,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: CommonConstant.defaultShadow),
              child: Center(
                child: Icon(
                  Icons.add,
                  size: 20,
                  color: AppColors.white,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
