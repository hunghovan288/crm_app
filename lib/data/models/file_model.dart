class FileModel {
  final String icon;
  final String name;
  final String size;
  final String dateTime;

  FileModel({this.icon, this.name, this.size, this.dateTime});
}
