class ProjectModel {
  final String name;
  final String description;
  final DateTime dateTime;
  final double percent;
  final int status;

  ProjectModel({
    this.name,
    this.description,
    this.dateTime,
    this.percent,
    this.status,
  });
}
