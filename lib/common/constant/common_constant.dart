import 'package:flutter/material.dart';

class CommonConstant {
  static const double defaultHeightKeyBoard = 175;

  static List<BoxShadow> defaultShadow = [
    BoxShadow(
        color: Colors.black.withOpacity(0.1), spreadRadius: 3, blurRadius: 5)
  ];

  // status project
  static const int PROJECT_TODO = 1;
  static const int PROJECT_DOING = 2;
  static const int PROJECT_DONE = 3;
  static const int PROJECT_PENDING = 4;
}
