class IconConstant {
  static const String path = 'assets/icons/';

  // container screen
  static const String dashboard = '${path}dashboard.svg';
  static const String calender = '${path}calender.svg';
  static const String note = '${path}note.svg';
  static const String personal = '${path}personal.svg';

  // common
  static const String back = '${path}back.svg';
  static const String iconClose = '${path}icon_close.svg';
  static const String imagePlaceHolder = '${path}icon_close.svg';

  // dashboard screen
  static const String excel = '${path}excel.svg';
  static const String word = '${path}word.svg';
  static const String powerpoint = '${path}powerpoint.svg';
  static const String file = '${path}file.svg';
}
