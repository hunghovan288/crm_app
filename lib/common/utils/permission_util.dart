import 'package:crm_app/common/utils/log_util.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionUtil {
  static Future<bool> isPermissionGranted(Permission permission) async {
    final status = await permission.request();
    LOG.d('PermissionUtil: $status');
    return status.isGranted;
  }

}