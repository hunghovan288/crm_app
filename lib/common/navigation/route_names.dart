class RouteName {
  static const String initial = '/';
  static const String dashBoardScreen = 'dashBoardScreen';

  static const String researchWidget = 'researchWidget';
}
